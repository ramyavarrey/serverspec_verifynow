require 'spec_helper'


describe service('cron') do
  it { should be_running }
end

describe file('/etc/passwd') do
  it { should be_file }
end

describe package('apache2') do
  it { should be_installed }
end

describe service('apache2') do
  it { should be_enabled }
  it { should be_running }
end
